name: 'LTP lite'
universal_id: ltp
origin: suites_zip
location: distribution/ltp/lite
maintainers:
  - name: Memory Management
    email: mm-qe@redhat.com
    gitlab: mm-qe
max_duration_seconds: 14400
cases:
  default:
    enabled:
      not:
        trees:
          or:
            - upstream|rawhide|eln|autosd-rhivos-rt
    target_sources:
      - arch/.*/include/asm/memblock\.h
      - arch/.*/include/asm/ptdump\.h
      - arch/.*/include/asm/vmalloc\.h
      - include/asm-generic/hugetlb\.h
      - include/linux/backing-dev\.h
      - include/linux/bootmem_info\.h
      - include/linux/compaction\.h
      - include/linux/hugetlb\.h
      - include/linux/hugetlb_cgroup\.h
      - include/linux/hugetlb_inline\.h
      - include/linux/ksm\.h
      - include/linux/list_lru\.h
      - include/linux/memblock\.h
      - include/linux/mempolicy\.h
      - include/linux/memfd\.h
      - include/linux/migrate\.h
      - include/linux/migrate_mode\.h
      - include/linux/mmap_lock\.h
      - include/linux/mmzone\.h
      - include/linux/oom\.h
      - include/linux/page_counter\.h
      - include/linux/page_idle\.h
      - include/linux/pagewalk\.h
      - include/linux/percpu\.h
      - include/linux/ptdump\.h
      - include/linux/rmap\.h
      - include/linux/swap\.h
      - include/linux/swap_cgroup\.h
      - include/linux/swap_slots\.h
      - include/linux/swapfile\.h
      - include/linux/vmacache\.h
      - include/linux/vmalloc\.h
      - include/linux/vmstat\.h
      - include/trace/events/migrate\.h
      - include/trace/events/oom\.h
      - include/uapi/asm-generic/hugetlb_encode\.h
      - include/uapi/linux/fadvise\.h
      - include/uapi/linux/oom\.h
      - mm/backing-dev\.c
      - mm/bootmem_info\.c
      - mm/compaction\.c
      - mm/fadvise\.c
      - mm/filemap\.c
      - mm/hugetlb_vmemmap\.c
      - mm/hugetlb_cgroup\.c
      - mm/hugetlb\.c
      - mm/hugetlb_vmemmap\.h
      - mm/hwpoison-inject\.c
      - mm/init-mm\.c
      - mm/ksm\.c
      - mm/list_lru\.c
      - mm/madvise\.c
      - mm/memblock\.c
      - mm/memfd\.c
      - mm/mempolicy\.c
      - mm/migrate\.c
      - mm/migrate_device\.c
      - mm/mincore\.c
      - mm/mlock\.c
      - mm/mm_init\.c
      - mm/mmap\.c
      - mm/mmap_lock\.c
      - mm/mmu_gather\.c
      - mm/mmzone\.c
      - mm/mprotect\.c
      - mm/msync\.c
      - mm/oom_kill\.c
      - mm/page-writeback\.c
      - mm/page_alloc\.c
      - mm/page_counter\.c
      - mm/page_idle\.c
      - mm/page_isolation\.c
      - mm/page_vma_mapped\.c
      - mm/pagewalk\.c
      - mm/percpu-vm\.c
      - mm/percpu\.c
      - mm/process_vm_access\.c
      - mm/ptdump\.c
      - mm/readahead\.c
      - mm/rmap\.c
      - mm/shmem\.c
      - mm/sparse\.c
      - mm/swap\.c
      - mm/swap_cgroup\.c
      - mm/swap_slots\.c
      - mm/swap_state\.c
      - mm/swapfile\.c
      - mm/truncate\.c
      - mm/userfaultfd\.c
      - mm/util\.c
      - mm/vmacache\.c
      - mm/vmalloc\.c
      - mm/vmscan\.c
      - mm/vmstat\.c
      - mm/workingset\.c
      - mm/zbud\.c
    trigger_sources:
      - arch/.*
      - fs/.*
      - include/.*
      - kernel/.*
      - lib/.*
      - mm/.*
    cases:
      baremetal:
        sets:
          - kt0
          - kt1
          - mem
        name: bare_metal
        host_types: bare_metal
      vm:
        enabled:
          not:
            or:
              # no suitable s390x with <hypervisor op="!=" value=""/>
              - trees: rhel6.*
                arches: s390x
        sets:
          - cloud
          - kt0
          - kt1
          - mem
        name: vm
        host_types: vm
  rhivos:
    name: 'AUTOSD - RHIVOS'
    enabled:
      trees:
        - autosd-rhivos-rt
    sets:
      - cloud
      - automotive
    host_types: normal
    environment:
      # xattr04 is for xfs *only*
      SKIPTESTS: getxattr04

  x86_64:
    high_cost: true
    enabled:
      arches: x86_64
      not:
        trees:
          or:
            - upstream|rawhide|eln
    cases:
      amd:
        name: amd
        host_types: amd
        trigger_sources:
          - arch/.*amd.*
      intel:
        name: intel
        host_types: intel
        trigger_sources:
          - arch/.*intel.*

  storage:
    high_cost: true
    enabled:
      arches: x86_64
      trees:
        - rhel[789].*|c9s.*
    cases:
      lpfc:
        name: lpfc
        host_types: lpfc
        target_sources:
          - drivers/scsi/lpfc/.*

      qla2xxx:
        name: qla2xxx
        host_types: qla2xxx
        target_sources:
          - drivers/scsi/qla2xxx/.*

      qedf:
        name: qedf
        host_types: qedf
        target_sources:
          - drivers/scsi/qedf/.*
          - include/linux/qed/.*\.h

      megaraid_sas:
        name: megaraid_sas
        target_sources:
          - drivers/scsi/megaraid/.*
        cases:
          gen3:
            name: gen3
            host_types: megaraid_sas_gen3

          gen3_5:
            name: gen3_5
            host_types: megaraid_sas_gen3_5

      mpt3sas:
        name: mpt3sas
        target_sources:
          - drivers/scsi/mpt3sas/.*
        cases:
          gen1:
            name: gen1
            host_types: mpt3sas_gen1

          gen2:
            name: gen2
            host_types: mpt3sas_gen2
